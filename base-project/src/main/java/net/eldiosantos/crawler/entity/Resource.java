package net.eldiosantos.crawler.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

/**
 * Created by esjunior on 17/05/2016.
 */
@Data
@Builder
public class Resource {
    @NonNull
    private String resourceId;
    private String content;
}
