package net.eldiosantos.crawler.test;

import net.eldiosantos.crawler.Crawler;
import net.eldiosantos.crawler.persistence.ResourceRepository;
import net.eldiosantos.crawler.persistence.utils.CreateTables;

import java.net.URI;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by esjunior on 18/05/2016.
 */
public class TestClass {
    public void test(final URI startPath, final Connection connection) throws SQLException {
        System.out.println(String.format("looking for files in: '%s'", startPath.toString()));
        new Crawler(new ResourceRepository(connection)).fetchToDatabase(startPath);
        connection.commit();

        System.out.println();
        System.out.println();
        System.out.println("#############################");
        System.out.println("## LISTING SAVED RESOURCES ##");
        System.out.println("#############################");
        System.out.println();

        new ResourceRepository(connection).list()
                .parallelStream()
                .forEach(r-> System.out.println(String.format("found resource '%s'", r.getResourceId())));
    }
}
