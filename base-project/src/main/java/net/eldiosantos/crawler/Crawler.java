package net.eldiosantos.crawler;

import net.eldiosantos.crawler.entity.Resource;
import net.eldiosantos.crawler.fetcher.FetcherFinder;
import net.eldiosantos.crawler.persistence.ResourceRepository;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by esjunior on 17/05/2016.
 */
public class Crawler {

    private final FetcherFinder finder;

    public Crawler(final ResourceRepository repository) {
        this.finder = new FetcherFinder(repository);
    }

    public Crawler(final FetcherFinder finder) {
        this.finder = finder;
    }

    public List<Resource>fetch(final URI startPath) {
        final List<Resource>result = new ArrayList<>();
        finder.getFetcher(startPath)
                .ifPresent(f->result.addAll(f.fetch(startPath).collect(Collectors.toList())));

        return result;
    }

    public void fetchToDatabase(final URI startPath) {
        finder.getFetcher(startPath)
                .ifPresent(f->f.fetchToDatabase(startPath));

    }
}
