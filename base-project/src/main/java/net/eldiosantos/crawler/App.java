package net.eldiosantos.crawler;

import net.eldiosantos.crawler.persistence.ResourceRepository;
import net.eldiosantos.crawler.persistence.utils.CreateTables;
import net.eldiosantos.crawler.test.TestClass;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) throws URISyntaxException, SQLException, IOException {
        final Connection connection = DriverManager.getConnection("jdbc:h2:file:./base-project/target/database.db", "user", "pass");
        connection.setAutoCommit(true);

        try {
            new ResourceRepository(connection).list();
        } catch (Exception e) {
            new CreateTables(connection).create();
        }
        final URI startPath1 = Paths.get(new File(".").getCanonicalPath() + "/base-project/target").toUri();

        new TestClass().test(startPath1, connection);

        final URI startPath2 = URI.create("https://www.google.com.br");
        new TestClass().test(startPath2, connection);
    }
}
