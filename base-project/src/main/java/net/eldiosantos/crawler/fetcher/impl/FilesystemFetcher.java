package net.eldiosantos.crawler.fetcher.impl;

import net.eldiosantos.crawler.entity.Resource;
import net.eldiosantos.crawler.fetcher.ResourceFetcher;
import net.eldiosantos.crawler.persistence.ResourceRepository;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by esjunior on 17/05/2016.
 */
public class FilesystemFetcher extends ResourceFetcher {
    public FilesystemFetcher(ResourceRepository repository) {
        super(repository);
    }

    @Override
    public Stream<Resource> fetch(final URI location) {

        final File file = new File(location);
        if(file.isDirectory()) {
                return Arrays.asList(file.listFiles())
                    .parallelStream()
                    .flatMap(f->FilesystemFetcher.this.fetch(f.toURI()));
        } else {
            final List<Resource>result = new ArrayList<>();
            buildOptionalResource(location).ifPresent(result::add);
            return result.stream();
        }
    }

    @Override
    public Boolean canFetch(final URI location) {
        return location.toASCIIString().toLowerCase().startsWith("file:");
    }

    @Override
    protected Resource buildResource(final URI location) {
        try (BufferedReader reader = new BufferedReader(new FileReader(location.getPath()))){

            return Resource.builder()
                .resourceId(location.toASCIIString())
                .content(
                    reader
                        .lines()
                        .collect(Collectors.joining())
                    ).build();
        } catch (Exception e) {
            new IllegalStateException(String.format("Error trying to reach resource '%s'", location.toASCIIString()), e).printStackTrace();
            return null;
        }
    }
}
