package net.eldiosantos.crawler.fetcher.factory;

import net.eldiosantos.crawler.fetcher.ResourceFetcher;

/**
 * Created by esjunior on 17/05/2016.
 */
public interface FetcherFactory<T extends ResourceFetcher> {
    public T build();
}
