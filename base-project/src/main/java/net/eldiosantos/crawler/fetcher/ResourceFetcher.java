package net.eldiosantos.crawler.fetcher;

import net.eldiosantos.crawler.entity.Resource;
import net.eldiosantos.crawler.persistence.ResourceRepository;

import java.net.URI;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by esjunior on 17/05/2016.
 */
public abstract class ResourceFetcher {

    private final ResourceRepository repository;

    private static FetcherFinder finder;

    protected ResourceFetcher(ResourceRepository repository) {
        this.repository = repository;
    }

    public abstract Stream<Resource> fetch(URI location);
    public abstract Boolean canFetch(URI location);

    protected abstract Resource buildResource(URI location);

    public Optional<ResourceFetcher> getFetcher(final URI path) {
        return finder.getFetcher(path);
    }

    public void fetchToDatabase(URI location) {
        fetch(location)
                .peek(l-> System.out.println(String.format("looking at: '%s'", l.getResourceId())))
                .forEach(r->{
                    try {
                        repository.persist(r);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
    }

    protected Optional<Resource> buildOptionalResource(final URI location) {
        return Optional.ofNullable(buildResource(location));
    }

    @Override
    public boolean equals(Object obj) {
        return getClass().getCanonicalName().equals(obj.getClass().getCanonicalName());
    }

    @Override
    public int hashCode() {
        return getClass().getCanonicalName().hashCode();
    }
}
