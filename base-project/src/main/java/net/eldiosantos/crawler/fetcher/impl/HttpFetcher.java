package net.eldiosantos.crawler.fetcher.impl;

import net.eldiosantos.crawler.entity.Resource;
import net.eldiosantos.crawler.fetcher.ResourceFetcher;
import net.eldiosantos.crawler.persistence.ResourceRepository;
import org.jsoup.Jsoup;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by esjunior on 18/05/2016.
 */
public class HttpFetcher extends ResourceFetcher {

    public HttpFetcher(ResourceRepository repository) {
        super(repository);
    }

    @Override
    public Stream<Resource> fetch(URI location) {
        System.out.println(String.format("looking for resource: '%s'", location.toASCIIString()));
        final List<Resource>result = new ArrayList<>();
        buildOptionalResource(location)
            .ifPresent(r-> {
                result.add(r);
                result.addAll(
                    Jsoup.parse(r.getContent())
                        .body()
                        .getElementsByTag("a")
                        .parallelStream()
                        .flatMap(link->{
                            final String href = link.attr("href");
                            try {
                                return fetch(URI.create(href));
                            } catch (Exception e) {
                                new IllegalArgumentException(String.format("Errot trying to fetch resource '%s'", href), e).printStackTrace();
                                return null;
                            }
                        })
                        .collect(Collectors.toSet())
                );
            });
        return result.stream();
    }

    @Override
    public Boolean canFetch(URI location) {
        return location.toASCIIString().toLowerCase().startsWith("http");
    }

    @Override
    protected Resource buildResource(URI location) {
        try(final BufferedReader reader = new BufferedReader(new InputStreamReader((InputStream) location.toURL().getContent()))) {
            return Resource.builder()
                .resourceId(location.toASCIIString())
                .content(reader.lines().collect(Collectors.joining()))
                .build();
        } catch (Exception e) {
            new IllegalArgumentException(String.format("Error trying to fetch resource '%s'", location.toString()), e).printStackTrace();
            return null;
        }
    }
}
