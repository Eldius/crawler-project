package net.eldiosantos.crawler.fetcher;

import net.eldiosantos.crawler.fetcher.ResourceFetcher;
import net.eldiosantos.crawler.persistence.ResourceRepository;
import org.reflections.Reflections;

import java.net.URI;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by esjunior on 17/05/2016.
 */
public class FetcherFinder {
    private static final Set<ResourceFetcher> fetchers = new HashSet<>();

    public FetcherFinder(ResourceRepository repository) {
        synchronized (fetchers) {
            if(fetchers.isEmpty()) {
                fetchers.addAll(
                    new Reflections().getSubTypesOf(ResourceFetcher.class).stream()
                        .map(c -> {
                            try {
                                return c.getDeclaredConstructor(ResourceRepository.class).newInstance(repository);
                            } catch (Exception e) {
                                e.printStackTrace();
                                return null;
                            }
                        }).collect(Collectors.toSet())
                );
                System.out.println();
                System.out.println();
                System.out.println("#############################");
                System.out.println("##      FETCHERS LIST      ##");
                System.out.println("#############################");
                System.out.println();
                fetchers.forEach(f-> System.out.println(String.format("\t* '%s'", f.getClass().getCanonicalName())));
                System.out.println("-----------------------------");
                System.out.println();
            }
        }
    }

    public synchronized Optional<ResourceFetcher> getFetcher(final URI path) {
        return fetchers.parallelStream()
                .filter(f->f.canFetch(path))
                .findAny();
    }
}
