package net.eldiosantos.crawler.persistence.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by esjunior on 18/05/2016.
 */
public class CreateTables {
    private final Connection connection;

    public CreateTables(Connection connection) {
        this.connection = connection;
    }

    public void create() throws SQLException {
        final Statement st = connection.createStatement();
        st.addBatch("create table RESOURCE (RESOURCE_ID VARCHAR(255) PRIMARY KEY, CONTENT BLOB);");
        st.executeBatch();
    }
}
