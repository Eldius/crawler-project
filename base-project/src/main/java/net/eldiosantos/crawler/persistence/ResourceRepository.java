package net.eldiosantos.crawler.persistence;

import net.eldiosantos.crawler.entity.Resource;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by esjunior on 18/05/2016.
 */
public class ResourceRepository {
    private static final String LIST_ELEMENT_SQL = "SELECT RESOURCE_ID, CONTENT FROM RESOURCE";
    private static final String FIND_ELEMENT_SQL = "SELECT RESOURCE_ID, CONTENT FROM RESOURCE WHERE RESOURCE_ID = ?";
    private static final String UPDATE_ELEMENT_SQL = "UPDATE RESOURCE SET CONTENT = ? WHERE RESOURCE_ID = ?";
    private static final String INSERT_ELEMENT_SQL = "INSERT INTO RESOURCE (RESOURCE_ID, CONTENT) VALUES(?, ?)";
    private final Connection connection;

    public ResourceRepository(Connection connection) {
        this.connection = connection;
    }

    public List<Resource>list() {
        try {
            final PreparedStatement st = connection.prepareStatement(LIST_ELEMENT_SQL);
            final ResultSet rs = st.executeQuery();
            final List<Resource>results = new ArrayList<>();
            while (rs.next()) {
                results.add(createResourceFromResultset(rs));
            }
            return results;
        } catch (Exception e) {
            throw new IllegalArgumentException("Error trying to list the resources", e);
        }
    }

    public void persist(final Resource resource) {
        final Resource result = find(resource.getResourceId());
        if(result != null) {
            update(resource);
        } else {
            insert(resource);
        }
    }

    private void insert(Resource resource) {
        try {
            final PreparedStatement st = connection.prepareStatement(INSERT_ELEMENT_SQL);
            final ByteArrayInputStream in = new ByteArrayInputStream(resource.getContent().getBytes());
            st.setString(1, resource.getResourceId());
            st.setCharacterStream(2, new InputStreamReader(in));
            st.execute();
        } catch (Exception e) {
            final Resource r = find(resource.getResourceId());
            throw new IllegalArgumentException(String.format("Error inserting resource '%s'", resource.getResourceId()), e);
        }
    }

    private void update(Resource resource) {
        try {
            final PreparedStatement st = connection.prepareStatement(UPDATE_ELEMENT_SQL);
            final ByteArrayInputStream in = new ByteArrayInputStream(resource.getContent().getBytes());
            st.setCharacterStream(1, new InputStreamReader(in));
            st.setString(2, resource.getResourceId());
            st.executeUpdate();
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("Error trying to update the resource '%s'", resource.getResourceId()), e);
        }
    }

    private Resource find(final String resourceId) {
        try {
            final PreparedStatement st = connection.prepareStatement(FIND_ELEMENT_SQL);
            st.setString(1, resourceId);
            final ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return createResourceFromResultset(rs);
            }
            return null;
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("Error looking for the resource '%s'", resourceId), e);
        }
    }

    private Resource createResourceFromResultset(ResultSet rs) {
        try {
            return Resource.builder()
                    .resourceId(rs.getString("RESOURCE_ID"))
                    .content(
                            new BufferedReader(rs.getCharacterStream("CONTENT"))
                                    .lines()
                                    .collect(Collectors.joining("\n"))
                    )
                    .build();
        } catch (Exception e) {
            throw new IllegalArgumentException("Error trying to build resource from resultset", e);
        }
    }
}
